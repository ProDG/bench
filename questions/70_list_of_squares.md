## Question

Which statement produces a list of squares?

## Answers:

1. `squares = [x**2 for x in range(10)]`
2. `squares = []`  
   `for x in range(10):`  
   `    squares.push(x*x)`
3. `squares = []`  
   `for x in range(10):`  
   `    squares[x] = x**x`
4. `squares = []`  
   `for x in range(10):`  
   `    squares[x] = x*x`
5. `squares = []`  
   `for x in range(10):`  
   `    squares[x] = x**2`

## Correct:

> @1. `squares = [x**2 for x in range(10)]`
