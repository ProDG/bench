## Sample code:

```python
def make_incrementer(n):        # Line A
    def increment(x):           # Line B
        return x + n            # Line C
    return increment            # Line D

add1 = make_incrementer(1)      # Line E
print(add1(3))                  # Line F
```

## Question:

Which line in the sample code above causes Python to define incrementer function?

## Answers:

1. `Line A`
2. `Line B`
3. `Line D`
4. `Line E`
5. `Line F`

## Correct:

> @2. `Line B`

## Hint:

I'm not actually sure.
