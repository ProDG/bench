## Sample code:
```python

fo = open("data.dat", "r")
fo.seek(-1, 2)
```
```text
# ==== text in data.dat ====
line 1
1234567890
abcdefghjklmnopqrstuvwxyz
ABCDEFGHJKLMNOPQRSTUVWXYZ
!@#$%^&*()

line last
# ==== end of data.dat text ====
```

## Question:

Why does the sample code above provoke an `io.UsupportedOperation` error?

## Answers:

1. In the text files opened with a `b` in the mode string, 
   only seeks relative to the beginning of the file are allowed.
2. In the binary files opened without a `b` in the mode string, 
   only seeks relative to the beginning of the file are allowed.
3. In the binary files opened with a `b` in the mode string, 
   only seeks relative to the beginning of the file are allowed.
4. In the text files opened without a `b` in the mode string, 
   only seeks relative to the beginning of the file are allowed.
5. In the unicode files opened without a `u` in the mode string, 
   only seeks relative to the beginning of the file are allowed.
   
## Correct:
>@4. In the text files opened without a `b` in the mode string, 
>    only seeks relative to the beginning of the file are allowed.



