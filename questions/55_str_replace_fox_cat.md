## Sample code:

```python
str = "The quick brown fox jumps over the lazy dog"

str.replace("fox", "cat")  # Line A

print(str)  # "The quick brown fox jumps over the lazy dog"
```

## Question:

In the sample code above, which code do you replace the line labeled as `Line A` with to produce the expected
output where fox is replaced by cat?

## Answers:

1. `x = str.replace("fox", "cat")`
2. `str.replace("fox", "cat").save()`
3. `str.replace("cat", "fox")`
4. `print(str.replace("cat", "fox"))`
5. `str = str.replace("fox", "cat")`

## Correct:
> @5. `str = str.replace("fox", "cat")`

