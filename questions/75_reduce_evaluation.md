## Sample code:
```python
from functools import reduce
X = lambda x: reduce(lambda y, z: y + z, range(1, x + 1))
print(X(5))
```

## Answers:

1. `((5 * (5 + 1)) / 2)`
2. `(1 + 2 + 3 + 4 + 5)`
3. `(5 + 4 + 3 + 2 + 1)`
4. `((((5 + 4) + 3) + 2) + 1)`
5. `((((1 + 2) + 3) + 4) + 5)`

## Correct:
> @5. `((((1 + 2) + 3) + 4) + 5)`

