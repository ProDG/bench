## Sample code:

```python
def f(a, lst=[]):
    lst.append(a)
    return lst
    
print(f(1))
print(f(2))
print(f(3))
```
## Question:

What is the output of the sample code above?

## Answers:

1. `[1]`  
   `[1, 2]`  
   `[1, 2, 3]`  
2. `(1)`  
   `(1, 2)`  
   `(1, 2, 3)`
3. `1`  
   `2`  
   `3`  
4. `[1]`  
   `[2]`  
   `[3]`  
5. `[1]`  
   `[2, 1]`  
   `[3, 2, 1]`  
   
   
## Correct:

> @1. `[1]`  
>     `[1, 2]`  
>     `[1, 2, 3]`  

## Hint:

Mutable data structures (as a list, in this case) should NOT be used for default argument values. Otherwise
we can meet side effects like here (values are accumulated in a list between calls).
