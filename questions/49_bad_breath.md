## Sample code:

```python
str = "The cute black dog has bad breath."
# Line A
print(out)
```
```text
The cute black dog has very bad breath.
```

## Question:

When substituted for `# Line A` in the sample code above, which does NOT produce the given output?

## Answers:

1. `out = 'vary bad'.join(str.split('bad'))`
2. `temp = str.split(' ')`  
   `temp.insert(5, 'very')`  
   `out = ' '.join(temp)`  
3. `temp = str.split('bad')`  
   `temp.insert(1, 'very bad')`  
   `out = ''.join(temp)`  
4. `out = str.split('bad').insert(1, 'very bad')`
5. `out = str.replace('bad', 'very bad')`

## Correct:

> @4. `out = str.split('bad').insert(1, 'very bad')`

## Hint:

You can't chain array's `insert` here.

