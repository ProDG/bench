## Sample code:
Both python files (modules) exist in the same directory from which the Python interpreter is run. 
```python
## start file taxhelper.py 

__all__ = ('method1', 'BClass') 

def method1(): 
    method2() 
    
    
class BClass: pass 


class TaxCalculator: 
    def __init__(self, name, tax_rate): 
        self._name = name 
        self._tax_rate = tax_rate 


def calculate(self, amount): 
    return self._tax_rate*amount 


def __str__(self): 
    return '%s Tax Calculator, using rate of %s' % (self._name, self._tax_rate) 

## end file taxhelper.py 
```

```python
## start file script1.py ##

from taxhelper import *
tc = TaxCalculator('Sales', 0.075) 

## end file script1.py ##
```

## Question:

Referring to the sample code above, why does the output show a `NameError`?

## Answers:

1. No `__export__` valiable was defined in `taxhelper.py`
2. Defining `__all__` in the module prohibits use of `from taxhelper import TaxCollector`.
3. No `__import__` variable was defined in `taxhelper.py`.
4. Defining `__all__` in the module limits symbols imported by `from taxhelper import *`
5. The use of `import *` is deprecated in Python 3.x.
 
## Correct:
 
> 4. Defining `__all__` in the module limits symbols imported by `from taxhelper import *`
