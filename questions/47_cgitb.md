## Sample code:

```python
import cgitb; cgitb.enable(display=0, logdir='.', format='')  # Line A

print("Content-Type: text/html")
print()
print('<h1>Hello World!</h1>')

x = 1 / 0
```

## Question:

Referring to the sample code above, what is the purpose of Line A?

## Answers:

1. Line A enables the script in HTML format to be ran in a CGI program designed to be called from a web browser 
   interface.
2. Line A invokes a special exception handler that logs traceback information to a file in the current directory 
   in HTML format.
3. Line A invokes a special exception handler that generates an html response with traceback information.
4. Line A invokes a special exception handler that logs traceback information to file in the current directory
   in text format.
5. Line A invokes a special logging handler that writes request information to a file in the current directory
   in text format.   

## Correct:

> @4. Line A invokes a special exception handler that logs traceback information to file in the current directory
>     in text format.
