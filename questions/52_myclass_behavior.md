## Sample code:

```python
class MyClass:
    def __init__(self, num=0):
        self.num = num
        
    def __setattr__(self, name, value):
        if type(value) == type(1):
            self.__dict__[name] = value
        else:
            raise ValueError
            
myclass = MyClass()            
```

## Question:

What is the behavior of `myclass` as defined in the sample code above?

## Answers:

1. `myclass` only allows integer attribute values to be assigned to it.
2. `myclass` raises an error if any additional attributes are created later than at instance creation.
3. Access to a `myclass` attribute coerces the attribute value to a numeric type.
4. `myclass` only allows the value of `1` to be assigned to its attributes.
5. Access to a `myclass` attribute raises an error if encountered in a non-numeric context.


## Correct:

> @1. `myclass` only allows integer attribute values to be assigned to it.
