## Sample code:

```python
a = 23
b = 17
quot = a // b
remain = a - (int(quot) * b)
qoutrem = (quot, remain)
del a, b, quot, remain
```

## Question:

Which is an equivalent way to perform the action in the sample code above?

## Answers

1. `quotrem = divmod(23, 17)`
2. `quot = 23/17`    
   `remain = 23%17`    
   `quotrem = (quot, remain)`    
   `del quot, remain`
3. `quotrem = (23%17, 23/17)`
4. `quotrem = (23/17 + 23%17)`
5. `quotrem = (div(23,17), mod(23, 17))`

## Correct:

> @1. `quotrem = divmod(23, 17)` 
