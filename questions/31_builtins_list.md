## Question:

Which do you use to build a list of the names of all built-in objects?

## Answers:

1. `import builtins, __main__`  
   `lst = [thing for thing in globlals.values()]`
2. `import builtins`  
   `lst = [thin for thin in builtins.__dict__.values()`
3. `import __main__`
   `lst = [for thin in __main__.__dict__.keys()]`
4. `import builtins`  
   `lst = builtins.__dict__`
5. `import builtins`  
   `lst = builtins.__dict__.keys()`
   
## Correct:
> @5. `import builtins`  
>     `lst = builtins.__dict__.keys()`

