## Sample code:

```python
def func(**kw):        # Line 1
    x = 1, 2, 3        # Line 2
    a, b, c = 1, 2, 3  # Line 3
    y = z              # Line 4
    d, e = 1, 2, 3     # Line 5
```

## Question:

Referring to the sample code above, which line contains an error, assuming `z` is defined?

## Answers:

1. Line 1
2. Line 2
3. Line 3
4. Line 4
5. Line 5

## Correct:
> @5. Line 5

## Hint:

Trying to assign 3 values into 2 variables, we'll get:  
`ValueError: too many values to unpack (expected 2)`
