## Question:

In the `unittest` library, which describes the relationships between assertions, tests, test cases and test suites?

## Answers:

1. A test should have one or more cases to be meaningful.  
   A test case should have one or more assertions to be meaningful.  
   A test case should have one or more test suites to be meaningful.  
2. A test should have one or more assertions to be meaningful.  
   A test case should have one or more tests to be meaningful.  
   A test suite should have one or more fixtures to be meaningful.  
3. A test should have one or more assertions to be meaningful.  
   A test case should have one or more tests to be meaningful.  
   A test should have one or more test suites to be meaningful.  
4. A test should have one or more test cases to be meaningful.  
   A test case should have one or more assertions to be meaningful.  
   A test suite should have one or more test cases to be meaningful.  
5. A test should have one or more assertions to be meaningful.  
   A test case should have one or more tests to be meaningful.  
   A test suite should have one or more test cases to be meaningful.  
   
   
## Correct:

> @5. A test should have one or more assertions to be meaningful.  
   A test case should have one or more tests to be meaningful.  
   A test suite should have one or more test cases to be meaningful.  


## Hint:

Test case is the class with tests (methods named `test_*`). Test suite is a set of tests.   
