## Sample code:
```python
a = set('abcdefgha')
b = set('qazxswedc')
```

## Question:

Based on the sample code above, which statement is `True`?

## Answers:

1. `len(a - b) == len(a) - len(b)`
2. `len(a + b) == len(a) + len(b)`
3. `len(a) == len(b) - 1`
4. `len(a & b) == len(a) + len(b)`
5. `len(a) == len(b)`

## Correct:

> @3. `len(a) == len(b) - 1`

## Hint:

Sets `a` and `b` are built from the same-length strings. But in the set `a` a letter `'a'` is duplicated 
(at the beginning and at the end). So length of the `a` will be 1 item smaller than `b`. 
