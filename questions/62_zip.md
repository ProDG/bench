## Sample code:
```python
z = zip([1, 2, 3], ['a', 'b', 'c'], ['A', 'B', 'C'])  # Line 1
for i in z: print(type(i))                            # Line 2
for i in z: print(type(i))                            # Line 3
```

## Question:

How does the above code show `z` is an iterator of three tuples?

## Answers:

1. `Line 2` reveals the type of each element of `z` list, and `Line 3` reveals `z` is an iterator 
since it is exhausted.
2. `Line 2` reveals the type of each element of `z` is iterable, and `Line 3` reveals `z` is a tuple 
since it is exhausted.
3. `Line 2` reveals the type of each element of `z` is tuple, and `Line 3` reveals `z` is an iterator 
since `z` is is not in memory.
4. `Line 2` reveals the type of each element of `z` is tuple, and `Line 3` reveals `z` is an iterator
since `z` is exhausted.
5. `Line 2` reveals the type of each element of `z` is tuple, and `Line 3` reveals `z` is iterable
since it is not exhausted.   

## Correct:

> @4. `Line 2` reveals the type of each element of `z` is tuple, and `Line 3` reveals `z` is an iterator
since `z` is exhausted.
