## Question:
Which file must exist in a directory for the contents of that directory to be considered a package?

## Answers:

1. `__package__.py`
2. `api.py`
3. `__init__.py`
4. `__modules__.py`
5. `modules.py`

## Correct:

> @3. `__init__.py`
