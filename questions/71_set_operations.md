## Question:

If `set1` and `set2` are sets, which is a valid operation?

## Answers:

1. `set1 / set2`
2. `set1 + set2`
3. `set1 * set2`
4. `set1 ^ set2`
5. `set1 ! set2`

## Correct:

> @4. `set1 ^ set2`

## Hint:

`set1 ^ set2` returns only items which are not in both sets.

It's equivalent to:

`set1.symmetric_difference(set2)`
