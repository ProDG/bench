## Sample code:

```python
str = 'This is a very long test'
out = str[:x] + ' not' + str[y:]
print(out)

# Output: This is not a very long test
```

## Question:

Base on the sample code above, which are the values of `x` and `y` to the output?


## Answers:

1. `x = 7; y = -18`
2. `x = 7; y = -17`
3. `x = 8; y = -17`
4. `x = 7; y = 8`
5. `x = 8; y = 8`

## Correct:

>@4. `x = 7; y = 8`
