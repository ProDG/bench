## Question:

Which expression evaluates to `True` when the substring in the variable `pat` occurs 
at least twice in the variable `s`?

## Answers:

1. `string.index(s[string.index(s, pat):], pat)`
2. `s.count(pat)`
3. `s.find(s[s.find(pat) + 1:], pat)`
4. `string.rfind(s, pat) > string.find(s, pat) > 0`
5. `0 <= s.find(pat) < s.rfind(pat)`

## Correct:
>@5. `0 <= s.find(pat) < s.rfind(pat)`
