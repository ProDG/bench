## Scenario:

A module in file `mymodule.py` is called directly by Python interpreter as a script to execute.

## Question

Based on the scenario above, what is the value of the `__name__` variable?

## Answers:

1. `mymodule`
2. `mymodule.main`
3. `__main__`
4. `mymodule.__main__`
5. `main`

## Correct:

> @3. `__main__`

