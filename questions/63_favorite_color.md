## Sample code:

```python
print(
    "My favorite color is %(color)-8s, my favorite animal is %(animal)s, "
    "and my favorite number is %(num)x" % {
        'animal': 'dog',
        'color': 'blue',
        'num': 16
    }
)
```

## Question:

What is the output of the sample code above?

## Answers:

1. `My favorite color is     blue, my favorite animal is dog, and my favorite number is 10`
2. `My favorite color is     blue, my favorite animal is dog, and my favorite number is 16`
3. `My favorite color is blue    , my favorite animal is dog, and my favorite number is 20`
4. `My favorite color is blue    , my favorite animal is dog, and my favorite number is 10`
5. `My favorite color is blue    , my favorite animal is dog, and my favorite number is 16`

## Correct:
> @4. `My favorite color is blue    , my favorite animal is dog, and my favorite number is 10`

## Hint:
