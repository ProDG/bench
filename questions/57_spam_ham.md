## Sample code:

```python
import spam.ham
```

## Question

Which code is equivalent to the sample code above?

## Answers

1. `spam = __import__('spam.ham', fromlist=['eggs'])`
2. `spam = __import__('spam.ham', fromlist=['ham'])`
3. `spam = __import__('spam', fromlist=['ham'])`
4. `spam = __import__('spam.ham', globals(), locals(), [], 0)`
5. `spam = __import__('spam', ['eggs'])`

## Correct:
> @4. `spam = __import__('spam.ham', globals(), locals(), [], 0)`

## Hint:

See `__import__` definition in `builtins.py`.
