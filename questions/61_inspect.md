## Sample code:

```python
import inspect

def foo(m):
    ''' <docstring> '''   # Line A
    for cls in inspect.getmro(type(m.__self__)):
        if m.__name__ in cls.__dict__: return cls
    return None
    
class MindStates(object):
    def happy(self):
        return 'fine'
    def sad(self):
        return 'like dirt'
    def soso(self):
        return 'all right'
        
class Person(MindStates):
    def __init__(self, who):
        MindStates.__init__(self)
        self.who = who
    def feels(self):
        print('{} feels {}.'.format(self.who, self.happy()))
    def soso(self):
        return 'not so good'
        
p = Person('Mary')
p.feels()

for m in inspect.getmembers(p, inspect.ismethod):
    cls = foo(m[1])
    print('{}\tis defined in class "{}" ({})'.format(
        m[0], cls.__name__, inspect.getsourcefile(m[1])
    )) 
```

## Question:

Which statement is a proper replacement for `<docstring>` at `Line A` in the sample code above?

## Answers:

1. Return the inherited class that re-defines the method.
2. Return the parent class that originally defines the instance method.
3. Return the class that defines the instance method.
4. Raise exception `None` if the requested method not found.
5. Return all classes that define the instance method.

## Correct:
> @1. Return the inherited class that re-defines the method.
