## Question:

Which method sets the metaclass of class `C` to `M`?

## Answers:

1. `class C(metaclass=M): pass`
2. `class C(meta=M): pass`
3. `class C: __metatype__=M`
4. `class C: __metaclass__=M`
5. `class C: __type__=M`

## Correct:

> @1. `class C(metaclass=M): pass`

Note: for Python 2 answer 4 is correct.
