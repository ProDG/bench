## Sample code:

```python
class MyType(type): pass

class SubType(MyType): pass

class MyObject(object):
    __metaclass__ = MyType
    
    
print("MyType.__class__ = %s", (MyType.__class__))
print("SubType.__class__ = %s", (SubType.__class__))
print("MyObject.__class__ = %s", (MyObject.__class__))    
```

## Question:

What is the output of the sample code above?

## Anwsers:

1. `MyType.__class__ = <type 'type'>`  
   `SubType.__class__ = <clas '__main__.MyType'>`  
   `MyObject.__class__ = <type 'object'>`
   
2. `MyType.__class__ = <type 'type'>`  
   `SubType.__class__ = <class '__main__.MyType'>`  
   `MyObject.__class__ = <class '__main__.MyType'>`
   
3. `MyType.__class__ = <type 'type'>`  
   `SubType.__class__ = <type 'type'>`  
   `MyObject.__class__ = <class 'object'>`
   
4. `MyType.__class__ = <type 'type'>`  
   `SubType.__class__ = <class '__main__.MyType'>`  
   `MyObject.__class__ = <class 'type'>`
   
5. `MyType.__class__ = <class 'type'>`  
   `SubType.__class__ = <class 'type'>`  
   `MyObject.__class__ = <class 'type'>`

## Correct:

>@ 5. `MyType.__class__ = <class 'type'>`  
>   `SubType.__class__ = <class 'type'>`  
>   `MyObject.__class__ = <class 'type'>`
