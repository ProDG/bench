## Scenario:

In the expression `X = n + m + p`: `n`, `m` and `p` consist of an integer, a floating point, and a complex number,
though not necessary in that order.

## Question:

Referring to the scenario above, after the assignment, you determine the type of value`X` contains is:

## Answers:

1. a complex number because of coercion.
2. the same type as the leftmost variable in the `sum(n)`
3. dependent on whether the complex number has an imaginary part that is non-zero.
4. the same type as the rightmost variable in the `sum(p)`
5. a floating point number because of coercion.

## Correct:
> @1. a complex number because of coercion.
