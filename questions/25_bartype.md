## Sample code:
```python
class BarType(object):
    def __init__(self):
        print('Bar', 'born', end=' - ')
        
    def __del__(self):
        print('Bar', 'died', end=' - ')
        
        
b = BarType()
c = b  # Line A
print('Deleting', end=' - ')
del b  # Line B
print('Ending', end= ' - ')


# Output with Line A: Bar born - Deleting - Ending - Bar Died -
# Output without Line A: Bar born - Deleting - Bar died - Ending -
```

## Question:

In the sample code above, why does `Line A` effect the output as shown?

## Answers:

1. `Line A` creates a new instance of `BarType` and therefore different output is produced.
2. You can only call `del` method only by executing `b.__del__`.
3. The reference count to `b` goes to zero after `Line B` when `Line A` is present.
4. The reference count to `b` goes to zero after `Line B` when `Line A` is absent.
5. A circular reference to `b` is created when `Line A` is absent.

## Correct:

> @4. The reference count to `b` goes to zero after `Line B` when `Line A` is absent.
