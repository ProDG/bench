```python
class Dog(object):
    def speak(self):
        print("woof!")
        
class Chihuahua(Dog):
    def speak(self):
        print("yip. yip.")
        # LINE A
        
Gunther = Chihuahua()
Gunther.speak()                
```

Output:

```text
yip. yip.
woof!
```

#### Question:

To produce the output shown above, which block of code do you use to replace `# LINE A`?

#### Answers:

1. `super(Chihuahua, self).speak()`
2. `super(self).speak()`
3. `super(Dog).speak()`
4. `super(Chihuahua).speak()`
5. `super(Dog.self).speak()`


#### Correct:

> 1. `super(Chihuahua, self).speak()`
