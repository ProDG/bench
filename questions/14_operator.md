```python
from operator import *

class PyObject:
    """Placeholder template class"""
    def __init__(self, __fakename__=None):
        if __fakename__: self.__fakename__ = __fakename__
        
class Indirect:
    def __init__(self):
        self.py_obj = PyObject(self.__class__.__name__)
        
    def __setattr__(self, name, value):
        if name == 'py_obj':
            self.__dict__[name] = value
        else:
            setattr(self.py_obj, name, value)
            
    def __getattr__(self, name):
        return getattr(self.py_obj, name)
        
    def __delattr__(self, name):
        del self.py_obj.__dict__[name]
        
indirect = Indirect()
indirect.pi = 3.1415        
```

## Question:
When the sample code above is executed, which expression has the same value as `indirec.pi`?

## Answers:

1. `getattr(indirect.py_obj.__dict__, 'pi')`
2. `indirect.__dict__['pi']`
3. `indirect.py_obj.__dict__.has_key('pi')`
4. `indirect.py_obj['pi']`
5. `getitem(indirect.py_obj.__dict__, 'pi')`

## Correct:

> @5. `getitem(indirect.py_obj.__dict__, 'pi')`
