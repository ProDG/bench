## Question:

Which pattern CANNOT contain an else block?

## Answers:

1. `for .. break`
2. `while .. continue`
3. `try .. finally`
4. `while .. break`
5. `try .. except`

## Correct:

> @3. `try .. finally`


