## Sample code:

```python
class MyError1(Exception): pass
class MyError2(MyError1): pass

try:
    raise MyError2()
except MyError1: print("MyError1")
except MyError2: print("MyError2")
except Exception: print('Exception')
```

## Question:

Why does `MyError1` get handled in the above code?

## Answers:

1. In a `try` statement with an except clause that mentions a particular class, that clause also handles
   any exception clauses from which it is derived.
2. Raising an error subclass is equivalent to sequential raising all error classes from which it inherits.
3. In a try statement with an except clause that mentions a particular class, that class raises all errors
   associated with its parent class.
4. Two exception clauses that are not related via subclassing are never equivalent, even if they have the
   same name.
5. In a try statement with an except clause that mentions a particular class, that clause also handles
   any exception classes derived from that class.

## Correct:
>@5. In a try statement with an except clause that mentions a particular class, that clause also handles
>    any exception classes derived from that class.
