```python
import multiprocessing as mp
import time

def sleeper(x):
    print('start', x)
    time.sleep(x / 10)
    print('end', x)
    return x
    

if __name__ == '__main__':
    pool = mp.Pool(processes=4)
    r = range(5, 0, -1)
    
    results = [pool.apply(sleeper, args=(x,)) for x in r]  # Line A
    # Line B
    print(results)  # Line C
```

#### Question:

Referring to the sample code above, if you want to demonstrate the asynchronous nature of multiprocessing, which
replacements do you use for Line A, B and C?

#### Answers:

##### 1.
```
Line A: results = [pool.map(sleeper, r)]
Line B: output = [p.get() for p in results]
Line C: print(output)
```

##### 2. 
```
Line A: results = [pool.map_async(sleeper, r)]
Line A: # blank
Line C: print(results)
```

##### 3.
```
Line A: results = [pool.apply_async(sleeper, args=(x,)) for x in r]
Line B: output = [p.get() for p in results]
Line C: print(output)
```

##### 4. 
```
Line A: results = [pool.apply_async(sleeper, args=(x,)) for x in r]
Line B: # blank
Line C: print(results)
```

##### 5. 
```
Line A: results = [pool.map_async(sleeper, args=(x,)) for x in r]
Line B: output = [p.get() for p in results]
Line C: print(output)
```

#### Correct:

>##### 3.
>```
>Line A: results = [pool.apply_async(sleeper, args=(x,)) for x in r]
>Line B: output = [p.get() for p in results]
>Line C: print(output)
>```
