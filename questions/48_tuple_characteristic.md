## Question:

Which is NOT a characteristic of tuples?

## Answers:

1. Tuples values can be changed.
2. Tuples are faster than lists.
3. Tuples cannot be changed after creation.
4. Tuples have a defined order.
5. Tuples can be used as keys in dictionaries.

## Correct:

> @1. Tuples values can be changed.
