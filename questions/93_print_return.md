## Sample code:

```python
def func1(x):
    print('f1')
    return (x + 1)

def func2(x):
    print('f2')
    return (x + 2)

def func3(x):
    print('f3')
    return (x + 3)

x = 3

print("OUTPUT A:")
if func1(x) < func2(x) < func3(x): pass
print("OUTPUT B:")
if func1(x) < func2(x) and func2(x) < func3(x): pass
```

## Question:

What is the output of the sample code above?

## Answers:

1. `OUTPUT A:f1f2f3`  
   `OUTPUT B:f1f2f3`
2. `OUTPUT A:f1f2f3`  
   `OUTPUT B:f1f2f2f3`
3. `SyntaxError`
4. `OUTPUT A:f1f2f2f3`  
   `OUTPUT B:f1f2f3`
5. `OUTPUT A:f1f2f2f3`
   `OUTPUT B:f1f2f2f3`
   
## Correct:

>@2. `OUTPUT A:f1f2f3`  
>    `OUTPUT B:f1f2f2f3`

