## Sample code:

```python
var1 = 0
var2 = 0
var3 = 0

with open('/tmp/tmp', 'w') as file:
    file.write("line 1\n")
    var1 = file.tell()
    file.close()

with open('/tmp/tmp', 'a') as file:
    file.write("line 2\n")
    var2 = file.tell()
    file.close()

with open('/tmp/tmp', 'a') as file:
    file.seek(var1)
    file.write("line 3\n")
    file.close()

with open('/tmp/tmp', 'a') as file:
    file.seek(var2, var3)
    file.write("line 4\n")
    file.close()
```

## Question:

What is the output of the tmp file of the sample code above?

## Answers:

(format is simplified)

1. `1 2 3 4`
2. `1 3 4`
3. `1 4`
4. `3 4`
5. `4`

## Correct:

> @1. `1 2 3 4`

## Hint:

In `'a'` mode, `seek()` is ignored.
