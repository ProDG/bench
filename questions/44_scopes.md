## Question:

Which lists valid scopes?

## Answers:

1. global, module, variable
2. built-in, class, instance
3. module, local, function
4. built-in, global, local
5. global, local, class

## Correct:

> @4. built-in, global, local
