## Question:
How can you make a variable accessible only within a function, but cause the value to persist across calls 
to that function?

## Answers:

1. Create a global variable at the beginning of the module, and add the `static` declaration to that variable within 
that function.
2. Create function attributes to emulate a static variable.
3. Create a global variable at the beginning of the module, and add `private` declaration to that variable within
the function.
4. Encapsulate access to the variable within `set_x()` and `get_x()` functions.
5. Add a positional argument, with an empty tuple as default value, to the end of the argument list.

## Correct:
>@2. Create function attributes to emulate a static variable.

(I'm not sure: it violates requirement 'accessible only within a function')
