## Question:

You use the `io.StringIO` module when:

## Answers:

1. performing string input output directly from files.
2. converting other types to `String` and vice versa.
3. treating a program generated string as if it were a diskfile.
4. transmitting a string over an open socket.
5. manipulating a diskfile's contents without using cumbersome `seek()/write()` operations.

## Correct:

> @3. treating a program generated string as if it were a diskfile.
