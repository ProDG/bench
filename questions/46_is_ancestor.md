## Question:

Which determines if class `A` is an ancestor of class `X`, assuming `X` inherits from `A`?

## Answers:

1. `isinstance(X, A)`
2. `isinstance(A, X)`
3. `issubclass(X, A)`
4. `inherits(X, A)`
5. `issubclass(X, 'A')`

## Correct:

> @3. `issubclass(X, A)`
