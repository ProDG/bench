## Sample code:

```python
a = 4
b = 7

x = lambda: a if 1 else b
lambda x: 'big' if x > 100 else 'small'

print(x())
```

## Question:

What is the output of the sample code above?

## Answers:

1. `4`
2. `7`
3. `big`
4. `small`
5. `lambda`

## Correct:

> @1. `4`
