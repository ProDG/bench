## Question:

Which is a valid Python exception block?

## Answers:

1. `try: x = x/y`  
   `unless ZeroDivisionError: pass`  
   `finally: pass`  
2. `try: x = x/y`  
   `except ZeroDivisionError: pass`  
   `except: pass`  
3. `try: x = x/y`  
   `catch ZeroDivisionError: pass`  
   `catch: pass`  
4. `throw: x = x/y`  
   `catch ZeroDivisionError: pass`  
   `catch: pass`  
5. `throw: x = x/y`  
   `except ZeroDivisionError: pass`  
   `finally: pass`  
   
## Correct:
> @2. `try: x = x/y`  
>     `except ZeroDivisionError: pass`  
>     `except: pass`  
