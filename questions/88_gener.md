## Sample code:

```python
def gener(b):
    b += 1
    yield b  # Line B

a = 0
g = gener(a)

# Line A
```

## Question:

Which do you use in place of `Line A` in the sample code to print the value of `b` from `Line B`?

## Answers:

1. `print(g.a)`
2. `print(g.__next__())`
3. `print(g.yield())`
4. `print(g.next)`
5. `print(g())`

## Correct:

>@2. `print(g.__next__())`
