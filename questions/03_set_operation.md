#### Question:
Which is a valid operation on a set?

#### Answers:

1. `myset.list()`
2. `myset.intersect(x)`
3. `myset.product()`
4. `myset.unionize(x)`
5. `myset.issuperset(x)`

#### Correct:

> 5. `myset.issuperset(x)`
