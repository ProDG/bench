## Scenario:

The module `mymodule.py` is located at `/home/user/python`, which is not in the path for Python modules.

## Question:

Based on the scenario above, which location specifies code that successfully loads `mymodule` in the Python script?

## Answers:

1. `import /home/user/python/mymodule`
2. `import "/home/user/python/mymoudule.py"`
3. `import os`  
   `os.environ['IMPORTPATH'] = r'/home/user/python'; import mymodule`
4. `import os`  
   `os.environ['PYTHONPATH'] = r'/home/user/python'; import mymodule`
5. `import sys`  
   `sys.path.append(r'/home/user/python'); import mymodule`
   
## Correct:

> @4. `import sys`  
>     `sys.path.append(r'/home/user/python'); import mymodule`   
