## Sample code:

```python
someglob = [chr(x + 65) for x in range(6)]

def outerFunc(cache={}, *args):
    outerVar = "some string"
    def innerFunc(*args):
        print(outerVar)
        print(someglob)
        print(args)
        print(cache)
    return innerFunc

outerFunc()()
```

## Question:

What is the output of the sample code above?

## Answers:

1. `None`  
   `None`  
   `None`  
   `[]`  
   `None`
2. `TypeError: 'NoneType' object is not callable`
3. `NameError: global name 'someglob' is not defined`
4. `NameError: global name 'outerVar' is not defined`
5. `some string`  
   `['A','B','C','D','E','F']`  
   `()`  
   `{}`

## Correct:
>@ 5. `some string`  
>     `['A','B','C','D','E','F']`  
>     `()` 
>     `{}`
