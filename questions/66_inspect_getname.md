## Sample code:

```python
import inspect

class A:
    def __init__(self, name):
        self.name = name
    
    def do_it(self):
        pass

    def get_name(self):
        return self.name

class B(A):
    def do_other(self):
        pass

    def get_name(self):
        return 'B(' + self.name + ')'

b = B('spam')

lst = inspect.getmembers(b, inspect.ismethod)  # Line A
for m in lst:
    print(m[0], end='  |  ')  # Line B

# Output: __init__  |  do_it  |  do_other  |  get_name  | 
```

## Question:

Based on the sample code above, how do you confirm that `get_name` method included is from class `B` and not class `A`?

## Answers:

1. Create and instance of class A and modify `Line A` to use it.
2. Change the name of the method in class `B` to `get_name_B` then run code again.
3. Add `print(b.get_name)`
4. Add `print(which(b.get_name))` to confirm.
5. Modify `Line B` so it also prints the second element of the `m` tuple.

## Correct: 
> @5. Modify `Line B` so it also prints the second element of the `m` tuple.

## Hint:
In the second element of the tuple it's visible to which class the method belongs:
```text
('do_it', <bound method A.do_it of <__main__.B object at 0x10bf115c0>>)
('do_other', <bound method B.do_other of <__main__.B object at 0x10bf115c0>>)
('get_name', <bound method B.get_name of <__main__.B object at 0x10bf115c0>>)
```

On other hand, answer 3. Add `print(b.get_name)` also is correct, as its results clearly show that
class `B` `get_name` implementation was called... 
