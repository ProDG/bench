## Question:

Which block of code stores a list of the file names in the current directory mathing the pattern `"*.mp3"` 
for a variable called `mp3files`?

## Answers:

1. `mp3files = glob('*.mp3)`
2. `import sys`   
   `mp3files = sys.find('*.mp3)`
3. `import glob`   
   `mp3files = glob.glob('*.mp3)`
4. `import os`   
   `mp3files = os.find_ext('mp3)`
5. `mp3files = file.match('*.mp3)`

## Correct:

>@3. `import glob`   
>    `mp3files = glob.glob('*.mp3)`

