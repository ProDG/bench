## Sample code:

```python
class A: pass

class B(A): pass

class C(object): pass

class D(C): pass

print(issubclass(C, (D, A, B, C)))
```

## Question:

Why is the output of the sample code above `True`?

## Answers:

1. The `issubclass` function uses an implicit `OR` operation.
2. All four classes are subclasses from `object`.
3. Class `A` has no subclasses, so classes `A` and `B` are super classes of all other classes.
4. Class `C` is subclass of `D` and that is sufficient to get `True` from `issublcass(C, (D, A, B, C))`
5. Class `A` is the superclass for the other three classes.

## Correct:

> @1. The `issubclass` function uses an implicit `OR` operation.

## Hint:

>@ 4. Class `C` is subclass of `D` and that is sufficient to get `True` from `issublcass(C, (D, A, B, C))`

— looks correct too, but `C` is subclass of `C` too, so option about `OR` looks more correct.

