```python
def MyFunction(val):
    if isinstance(val, int): return val + 1
    elif isinstance(val, str): return val + "1"
    elif isinstance(val, complex):
        # We will create a complex operation for next version
        raise ERROR1
    else:
        # The function should not be called with other types
        raise ERROR2
```

## Question:

Which built-in exceptions for `ERROR1` and `ERROR2` express the semantics of the the code and commens in the sample
code above?

## Answers:

1. `ERROR1=NotImplementedError`  
   `ERROR2=TypeError`
2. `ERROR1=NameError`   
   `ERROR2=NotImplementedError`
3. `ERROR1=ValueError`  
   `ERROR2=TypeError`
4. `ERROR1=ValueError`  
   `ERROR2=ValueError`
5. `ERROR1=SyntaxError`   
   `ERROR2=TypeError`

## Correct:

> 1. `ERROR1=NotImplementedError`   
>     `ERROR2=TypeError`
