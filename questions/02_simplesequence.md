```python
baselist = ['apple', 'orange', 'banana', 'kiwi', 'strawberry']
simpleseq = SimpleSequence(baselist)

simpleseq[0] == 'apple'             # Line 1
simpleseq[1] != 'banana'            # Line 2
simpleseq[2:-1] == baselist[2:-1]   # Line 3
len(simpleseq) == len(baselist)     # Line 4
```

#### Question:

Which is the class definition that creates a simple sequence, such that line 1, 2, 3 and 4 in the sample code above, 
all evaluate to `True`?

#### Answers: 

##### 1.
```python
class SimpleSequence:
    def __init__(self, data): self.data = data
    def __getitem__(self, i): return self.data[i]
    def __setitem__(self, i, val): self.data[i] = val
    def __delitem__(self, i): del self.data[i]
    def __len__(self): return len(self.data)
```
##### 2.
```python
class SimpleSequence(object):
    def __init__(self, data):
        self.data = data
```
##### 3.
```python
class SimpleSequence:
    def __init__(self, data): self.data = data
    def _getitem_(self, i): return self.data[i]
    def _setitem_(self, i, val): self.data[i] = val
    def _delitem_(self, i): del self.data [i]
    def _len_(self): return len(self.data)
```
##### 4.
```python

class SimpleSequence:
    def __init__(self, data) :self.data=data
    def __get_item__(self, i): return self.data [i]
    def __set_item__(self, i, val): self.data[i] = val
    def __del_item__(self, i): del self.data[i]
    def __len__(self): return len(self.data)
```
##### 5.
```python
class SimpleSequence:
    def __init__(self, data): self.data = data
    def get_item(self, i): return self.data[i]
    def set_item(self, i, val): self.data[i] = val
    def del_item(self, i) : del self.data[i]
    def len(self): return len(self.data)
```

#### Correct:

> @1.
> ```python
> class SimpleSequence:
>     def __init__(self, data): self.data = data
>     def __getitem__(self, i): return self.data[i]
>     def __setitem__(self, i, val): self.data[i] = val
>     def __delitem__(self, i): del self.data[i]
>     def __len__(self): return len(self.data)
>```
