## Question:

Which Python variable name is invalid?


## Answers:

1. `five_times_the_initial_quantity_should_be_stored_in_this_variable`
2. `_____5times`
3. `FIVETIMES`
4. `__fivetimes__`
5. `5times`

## Answer:

> @5. `5times`

## Hint:

Variable name can't start with a number.
