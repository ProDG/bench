## Sample code:

```python
import urllib.request
username = 'pytest'
password = 'pypass'
url = 'http://example.com/pytest/'
auth_handler = urllib.request.HTTPBasicAuthHandler()
# Line A
print(urllib.request.urlopen(url).read())
```

## Question:

Which code should you use in place of `Line A` in the sample code above to access URL protected 
by Basic HTTP authentication?

## Answers:

1. `auth_handler.add_password(realm=None, uri=url, user=username, passwd=password)`  
   `opener = urllib.request.build_opener(auth_handler)`  
   `urllib.request.install_opener(opener)`
2. `auth_handler.add_password(url, 'bbpython', username, password)`  
   `opener = urllib.request.build_opener(auth_handler)`  
   `urllib.request.install_opener(opener)`
3. `auth_handler.add_password(uri=url, user=username, passwd=password)`  
   `opener = urllib.request.build_opener(auth_handler)`  
   `urllib.request.install_opener(opener)`
4. `auth_handler.add_password(options=(realm:'bbpython', uri:url, user:username, passwd:password)`  
   `opener = urllib.request.build_opener(auth_handler)`  
   `urllib.request.install_opener(opener)`
5. `opener = urllib.request.build_opener(auth_handler)`
   `urllib.request.install_opener(opener)`  
   `auth_handler.add_password(realm=None, uri=url, user=username, passwd=password)`
   
## Correct:

>@1. `auth_handler.add_password(realm=None, uri=url, user=username, passwd=password)`  
>    `opener = urllib.request.build_opener(auth_handler)`  
>    `urllib.request.install_opener(opener)`  
