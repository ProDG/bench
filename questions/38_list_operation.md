## Question:

Which is a valid operation on a list?

## Answers:

1. `l1.delete(x)`
2. `l1.del(x)`
3. `l1.invert()`
4. `l1.remove(x)`
5. `l1.push(x)`

## Correct:

> @4. `l1.remove(x)`
