## Sample code:
```python
keys = ['key1', 'key2', 'key3', 'key4', 'key5']
vals = ['val1', 'val2', 'val3', 'val4', 'val5']

# Line A

print(mydict)

# Output: {'key1': 'val1', 'key2': 'val2', 'key3': 'val3', 'key4': 'val4', 'key5': 'val5'}
```

## Question:

Ignoring order, which do you use in place of `Line A` to product the output shown in the sample code above?

## Answers:

##### 1.
```python
mydict = dict()
for x in keys:
    mydict[x] = vals.pop()
```
##### 2.
```python
mydict = dict()
for x in range(len(keys)):
    mydict[keys[x]] = vals[x]
```
##### 3.
```python
mydict = dict()
mydict.fromkeys(keys, vals)
```
##### 4. 
```python
mydict = dict(keys.join(vals))
```
##### 5.
```python
mydict = dict(keys, vals)
```

## Correct:
##### 2.
```python
mydict = dict()
for x in range(len(keys)):
    mydict[keys[x]] = vals[x]
```

