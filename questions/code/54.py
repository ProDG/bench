def make_incrementer(n):        # Line A
    def increment(x):           # Line B
        return x + n            # Line C
    return increment            # Line D

add1 = make_incrementer(1)      # Line E
print(add1(3))                  # Line F
