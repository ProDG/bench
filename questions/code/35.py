class LikeInt:
    def __init__(self, value):
        self.value = value

    def __add__(self, other):
        return self.value + other

    def __radd__(self, other):
        return self.value + other

a = LikeInt(5)
b = 2

print(a + b)
print(b + a)  # __radd__ is required to have this operation working
