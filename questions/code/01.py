class Dog(object):
    def speak(self):
        print("woof!")


class Chihuahua(Dog):
    def speak(self):
        print("yip. yip.")
        super(Chihuahua, self).speak()


Gunther = Chihuahua()
Gunther.speak()
