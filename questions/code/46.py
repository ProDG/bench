class A:
    pass

class X(A):
    pass

print(issubclass(X, A))
