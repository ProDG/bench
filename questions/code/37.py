a = [
#    C  C  C
#    1  2  3
    [1, 2, 3],  # R 1
    [4, 5, 6],  # R 2
    [7, 8, 9]   # R 3
]

b = [[row[i] for row in a] for i in range(len(a))]

print(a)
print(b)
