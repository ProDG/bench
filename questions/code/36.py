z = 42

def func(**kw):        # Line 1
    x = 1, 2, 3        # Line 2
    a, b, c = 1, 2, 3  # Line 3
    y = z              # Line 4
    d, e = 1, 2, 3     # Line 5

print(func())
