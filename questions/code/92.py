import inspect

def callsig(function):
    """ Build a string with source code of the function call """
    desc = inspect.getargspec(function)
    sign = ','.join(desc[0])
    if desc[1]:
        sign += ',*' + desc[1]
    if desc[2]:
        sign += ',**' + desc[2]
    if sign and sign[0] == ',':
        sign = sign[1:]

    return sign

def f(arg1, arg2=None):
    pass

if __name__ == '__main__':
    print(callsig(f))
