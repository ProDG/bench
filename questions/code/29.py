set1 = set(['item_1', 'item_2', 'item_3', 'item_5', 'item_8'])
set2 = set(['item_2', 'item_3', 'item_5', 'item_7', 'item_11'])

print(sorted(set1.union(set2)))
print(sorted(set1.intersection(set2)))
print(sorted(set1.difference(set2)))
print(sorted(set1.symmetric_difference(set2)))
