import inspect

class A:
    def __init__(self, name):
        self.name = name

    def do_it(self):
        pass

    def get_name(self):
        return self.name


class B(A):
    def do_other(self):
        pass

    def get_name(self):
        return 'B(' + self.name + ')'


b = B('spam')

lst = inspect.getmembers(b, inspect.ismethod)  # Line A
for m in lst:
    print(m, end='  |  ')  # Line B

print(b.get_name())
# Output: __init__  |  do_it  |  do_other  |  get_name  |
