str1 = "This is the first string"
str2 = "This is the second string"

file1 = open('file1', mode='x')
file2 = open('file2', mode='x')
file3 = open('file3', mode='x')

print(str1, file=file1)
print(str1, file=file2)
print(str1, file=file3)

file1.close()
file2.close()
file3.close()

file1 = open('file1', mode='a')
file2 = open('file2', mode='+r')
file3 = open('file3', mode='r+')

print(str2, file=file1)
print(str2, file=file2)
print(str2, file=file3)

file1.close()
file2.close()
file3.close()
