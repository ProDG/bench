from unittest import TestCase


class AreaStrategy(object):
    def calculate(self, x, y):
        if x > 0 and y > 0: return x * y
        if x < 0 or y < 0: raise ValueError('Cannot calculate the area of any negative numbers')
        else: return 0


class TestAreaStrategy(TestCase):
    def setUp(self):
        self._width = 30
        self._height = 144
        self._strategy = AreaStrategy()

    def testCalculate(self):
        strat = self._strategy
        self.assertEqual(self._width * self._height, strat.calculate(self._width, self._height))
        self.assertEqual(0, strat.calculate(self._width, 0))
        self.assertRaises(ValueError, strat.calculate, -self._width, self._height)
        self.assertRaises(ValueError, strat.calculate, -self._width, 0)


if '__main__' == __name__:
    import unittest

    unittest.main()
