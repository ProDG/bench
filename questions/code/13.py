class A(object):
    def instance_monty(self, x):
        print("instance_monty: {}  arg: {}".format(self, x))

    @classmethod
    def class_brian(cls, x):
        print("class_brian: {}  arg: {}".format(cls, x))

    @staticmethod
    def static_wanda(x):
        print("static_wanda with arg {}".format(x))

A.class_brian('hello')
A.static_wanda('hello')
A.instance_monty(A, 'hey')
