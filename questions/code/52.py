class MyClass:
    def __init__(self, num=0):
        self.num = num

    def __setattr__(self, name, value):
        if type(value) == type(1):
            self.__dict__[name] = value
        else:
            raise ValueError


myclass = MyClass()

myclass.ha = 42

print(myclass.__dict__)

myclass.ho = 'hey'  # value error is risen
