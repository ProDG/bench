c = 1

def f(n):
    print(c + n)

def g(n):
    global c
    c = c + n
    print(c)

f(1)
g(1)
