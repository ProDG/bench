class SimpleSequence:
    def __init__(self, data): self.data = data
    def __getitem__(self, i): return self.data[i]
    def __setitem__(self, i, val): self.data[i] = val
    def __delitem__(self, i): del self.data[i]
    def __len__(self): return len(self.data)


baselist = ['apple', 'orange', 'banana', 'kiwi', 'strawberry']
simpleseq = SimpleSequence(baselist)


assert simpleseq[0] == 'apple'             # Line 1
assert simpleseq[1] != 'banana'            # Line 2
assert simpleseq[2:-1] == baselist[2:-1]   # Line 3
assert len(simpleseq) == len(baselist)     # Line 4
