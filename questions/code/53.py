from multiprocessing import Process, Pipe


def f(conn):
    conn.send('This is sent through a pipe!')
    conn.close()


def g(conn):
    conn.send('This is sent through a pipe too!')


if __name__ == '__main__':
    parent_conn, child1_conn = Pipe()
    parent_conn, child2_conn = Pipe()

    p = Process(target=f, args=(child1_conn,))
    q = Process(target=g, args=(child2_conn,))

    q.start()
    p.start()

    print(parent_conn.recv())

    p.join()
    q.join()
