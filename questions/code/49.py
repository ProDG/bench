str = "The cute black dog has bad breath."

print(1)
out = 'vary bad'.join(str.split('bad'))
print(out)

print(2)
temp = str.split(' ')
temp.insert(5, 'very')
out = ' '.join(temp)
print(out)

print(3)
temp = str.split('bad')
temp.insert(1, 'very bad')
out = ''.join(temp)
print(out)

print(4)
out = str.split('bad').insert(1, 'very bad')
print(out)


print(5)
out = str.replace('bad', 'very bad')
print(out)
