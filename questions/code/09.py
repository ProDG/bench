import multiprocessing as mp
import time


def sleeper(x):
    print('start', x)
    time.sleep(x / 10)
    print('end', x)
    return x


if __name__ == '__main__':
    pool = mp.Pool(processes=4)
    r = range(5, 0, -1)

    results = [pool.apply_async(sleeper, args=(x,)) for x in r]
    output = [p.get() for p in results]
    print(output)
