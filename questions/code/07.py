def gen():
    print('gen called')
    a, b = 0, 1
    print("START", end=" ")
    for i in range(4):
        print('iter', end='..')
        yield b
        a, b = b, a + b

    print("END", end=" ")


def rungen():
    G = gen()

    while True:
        try:
            v = next(G)
            print(v, end=" ")
        except StopIteration:
            break


rungen()  # Line A output START 1 1 2 3 END
print()
for v in gen(): print(v, end=" ")  # Line B output START 1 1 2 3 END
