for a in range(5):
    print(a)
    if a == 2:
        break
else:
    print('else')

a = 0
while a < 5:
    print(a)
    if a == 2:
        break
    a += 1
else:
    print('else')


try:
    a = 0
except Exception:
    print('exception')
else:
    print('else')

try:
    a = 0
except Exception:
    print('exception')
finally:
    print('finally')
else:
    print('else')
