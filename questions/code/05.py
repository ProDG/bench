import sys


class SuperClass:
    def __del__(self):
        print('supeclass __del__ was called')


class Klass(object):
    def __init__(self):
        print()

    def __del__(self):
        print('Klass __del__ method was called')

x = Klass()
print(x)

l = [x, 1]

print(sys.getrefcount(x))
print(sys.getrefcount(Klass))

del x

print(sys.getrefcount(Klass))
print(l)
print(sys.getrefcount(x))
