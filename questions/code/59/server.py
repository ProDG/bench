import socket
sskt = socket.socket()
host = '127.0.0.1'
port = 54321
sskt.bind((host, port))

sskt.listen(1)
c, addr = sskt.accept()
print('Connection from', addr)

c.send(b'Your connection succeeded.')

c.close()
