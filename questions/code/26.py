import warnings


def deprecated(func):
    def newFunc(*args, **kwargs):
        warnings.warn('Call to deplecated function %s' % (func.__name__), category=DeprecationWarning)
        return func(*args, **kwargs)

    return newFunc


#@deprecated  # Line A
def prod(x, y):
    return x * y

# - - - - - - - - - -  # Line B
prod = deprecated(prod)
print(prod(2, 3))
