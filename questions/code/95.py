def make_incrementer(n):        # Line 1
    def increment(x):           # Line 2
        return x + n            # Line 3
    return increment            # Line 4

add1 = make_incrementer(1)      # Line 5
print(add1(3))                  # Line 6
