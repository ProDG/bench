def func1(x):
    print('f1')
    return (x + 1)

def func2(x):
    print('f2')
    return (x + 2)

def func3(x):
    print('f3')
    return (x + 3)

x = 3

print("OUTPUT A:")
if func1(x) < func2(x) < func3(x): pass
print("OUTPUT B:")
if func1(x) < func2(x) and func2(x) < func3(x): pass
