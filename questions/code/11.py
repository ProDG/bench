def change_and_store(delta):
    if not hasattr(change_and_store, 'value'):
        change_and_store.value = 0

    change_and_store.value += delta
    print(change_and_store.value)

change_and_store(1)
change_and_store(-30)
change_and_store(3)
print(change_and_store.value)
change_and_store.value = 100500

change_and_store(0)
