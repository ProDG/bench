__all__ = ('method1', 'BClass')


def method1():
    method2()


class BClass: pass


class TaxCalculator:
    def __init__(self, name, tax_rate):
        self._name = name
        self._tax_rate = tax_rate


def calculate(self, amount):
    return self._tax_rate*amount


def __str__(self):
    return '%s Tax Calculator, using rate of %s' % (self._name, self._tax_rate)
