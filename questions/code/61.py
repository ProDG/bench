import inspect


def foo(m):
    ''' <docstring> '''  # Line A
    for cls in inspect.getmro(type(m.__self__)):
        if m.__name__ in cls.__dict__: return cls
    return None


class MindStates(object):
    def happy(self):
        return 'fine'

    def sad(self):
        return 'like dirt'

    def soso(self):
        return 'all right'


class Person(MindStates):
    def __init__(self, who):
        MindStates.__init__(self)
        self.who = who

    def feels(self):
        print('{} feels {}.'.format(self.who, self.happy()))

    def soso(self):
        return 'not so good'


p = Person('Mary')
p.feels()

for m in inspect.getmembers(p, inspect.ismethod):
    cls = foo(m[1])
    print(cls.__name__)
    print('{}\t\tis defined in class "{}" ({})'.format(
        m[0], cls.__name__, inspect.getsourcefile(m[1])
    ))
