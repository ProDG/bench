print(
    "My favorite color is %(color)-8s, my favorite animal is %(animal)s, "
    "and my favorite number is %(num)x" % {
        'animal': 'dog',
        'color': 'blue',
        'num': 16
    }
)
