i = [1, 2, 3, 4, 5]


def iter_idx_val(iterable):
    indices = range(len(iterable))
    for idx, val in zip(indices, iterable):
        yield idx, val

assert list(iter_idx_val(i)) == list(enumerate(i))
