## Sample code:

```python
from multiprocessing import Process, Pipe

def f(conn):
    conn.send('This is sent through a pipe!')
    conn.close()
    
def g(conn):
    conn.send('This is sent through a pipe too!')
    conn.close()
    
if __name__ == '__main__':
    parent_conn, child1_conn = Pipe()
    parent_conn2, child2_conn = Pipe()
    
    p = Process(target=f, args=(child1_conn,))
    q = Process(target=g, args=(child2_conn,))
    
    q.start()
    p.start()
    
    print(parent_conn.recv())
    print(parent_conn2.recv())
    
    p.join()
    q.join()
```

## Question: 

What is the output of the sample code above?

## Answers:

1. `This is sent through a pipe too!`  
   `BrokenPipeError`
2. `This is sent through a pipe!`  
   `BrokenPipeError`
3. `This is sent through a pipe too!`
4. `This is sent through a pipe!`  
   `This is sent through a pipe too!`
5. `This is sent through a pipe!`

## Correct:

> @1. `This is sent through a pipe too!`  
>   `BrokenPipeError`

## Hint:

to be figured out.
