```python
from functools import reduce
A = lambda y, z: y + z / 0
X = lambda x: reduce(A, range(4, x + 1))

print(X(4))
print(X(5))
```

## Question:

Based on the sample code above, why does `X(4)` evaluate, byt `X(5)` result in `ZeroDivisionError`?

## Answers:

1. Since the length of the range is 1 foe `X(4)`, only the `y` value for function `A` is present and `A(y) = y` is
used.
2. When `X(5)` is evaluated it handles `X(4)` first which results in the `ZeroDivisionError`.
3. Since the length of the range is 1 for `X(4)`, the function `A` is not called.
4. Since the length of the range is 1 for `X(4)`, no actual range is produced so the division by zero never occurs.
5. lambda functions are protected from runtime errors on their first execution.

## Correct:

> @3. Since the length of the range is 1 for `X(4)`, the function `A` is not called.
