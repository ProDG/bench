## Sample code:

```python
import re

def f1(data):
    p = re.compile('(?P<dept>[A-Z]{2, 3}) (?P<num>[0-9]{3})')
    return p.search(data)
```

## Question:

Which code extracts the matched data from the object returned by `f1` in the sample code above?

## Answers:

1. `dept, num = f1('CS 101')`
2. `obj = f1('CS 101')`  
   `dept, num = obj.group('dept'), obj.group('num')`
3. `obj = f1('CS 101')`  
   `dept, num = obj[0], obj[1]`
4. `obj = f1('CS 101')`  
   `dept, num = obj.get('dept'), obj.get('num')`
5. `obj = f1('CS 101')`  
   `dept, num = obj['dept'], obj['num']`   


## Correct: 

>@2. `obj = f1('CS 101')`  
>    `dept, num = obj.group('dept'), obj.group('num')`
