## Sample code:

```python
def countdown(i):
    print(i, end=" ")
    if i > 0: countdown(i - 1)

countdown(4)  # Output: 4 3 2 1 0
```

## Question:

Based on the sample code above, which makes the countdown function recursive?

## Answers:

1. The `countdown` function provides repetitive output.
2. The `countdown` function exists before it's function definition is completed.
3. The `countdown` function is memory inefficient and slow.
4. The `countdown` function defines itself
5. The `countdown` function calls itself.

## Correct:

>@5. The `countdown` function calls itself.
