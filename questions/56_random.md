## Question:

Which module is Python's standart library implementation of a pseudo-random number generator?

## Answers:

1. `random`
2. `random.generator`
3. `whrandom`
4. `rand`
5. `prnrandom`

## Correct:

> @1. `random`
