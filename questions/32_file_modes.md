## Sample code:

```python
str1 = "This is the first string"
str2 = "This is the second string"

file1 = open('file1', mode='x')
file2 = open('file2', mode='x')
file3 = open('file3', mode='x')

print(str1, file=file1)
print(str1, file=file2)
print(str1, file=file3)

file1.close()
file2.close()
file3.close()

file1 = open('file1', mode='a')
file2 = open('file2', mode='+r')
file3 = open('file3', mode='r+')

print(str2, file=file1)
print(str2, file=file2)
print(str2, file=file3)

file1.close()
file2.close()
file3.close()
```

## Question:

Which are the contents of the three files created by (... thr code above?)

## Answers:

1. `cat file1`  
   `This is the first string`  
   `This is the second string`  
   `cat file2`  
   `This is the first string`  
   `This is the second string`  
   `cat file3`  
   `This is the second string`  
2. `cat file1`  
   `This is the first string`  
   `This is the second string`  
   `cat file2`  
   `This is the second string`  
   `cat file3`  
   `This is the second string`  
3. `cat file1`  
   `This is the first string`  
   `This is the second string`  
   `cat file2`  
   `This is the first string`  
   `cat file3`  
   `This is the second string`  
4. `cat file1`  
   `This is the first string`  
   `This is the second string`  
   `cat file2`  
   `This is the first string`  
   `This is the second string`  
   `cat file3`  
   `This is the first string`  
   `This is the second string`  
   
## Correct:

> @2. `cat file1`  
>   `This is the first string`  
>   `This is the second string`  
>   `cat file2`  
>   `This is the second string`  
>   `cat file3`  
>   `This is the second string`  
