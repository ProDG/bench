```python
x = Klass()  # assume Klass is a class already defined
del x  # Line A
```

#### Question:

Which occurs at `# Line A` when the sample code above is executed?

#### Answers:

1. It decrements the reference count for `Klass` by one. If the new count value is 0, the `__del__` method 
is called on `Klass`
2. The `__del__` method defined in `Klass` or its base class is executed.
3. `SyntaxError`
4. It decrements the reference count for `x` by one. If the new count is 0, the `__del__` method is called on `x`.
5. The `__del__` method is called on the instance of `Klass(x)` and any exceptions are ignored.

#### Correct:

> 2. The `__del__` method defined in `Klass` or its base class is executed.

or

> 5. The `__del__` method is called on the instance of `Klass(x)` and any exceptions are ignored.

(I'm not sure!)

