## Sample code:
```python
class class1:
    a = 1

    def func1(self):
        a = 2
        class1.a += 1
        a += 1
        print(class1.a)
        print(a)

myclass = class1

class1.func1()
class1.func1()
```

## Question:

What is the output of the sample code above?

## Answers:

1. `2 2 2 2`
2. `2 3 2 3`
3. `2 3 3 3`
4. `2 3 3 4`
5. `4 4 4 4`

## Correct:
> @3. `2 3 3 3`
