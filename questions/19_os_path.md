```python
import os
from os.path import join, getsize

def f1(rootDir, exDir='CVS'):
    for root, dirs, files in os.walk(rootDir):
        print(root, "consumes")
        print(sum(getsize(join(root, name)) for name in files),)
        print("bytes in", len(files), "files")
        if exDir in dirs:
            dirs.remove(exDir)

if '__main__' == __name__: f1('/home/user/work', '.svn')  # Line A
```

## Question:

Based on the sample code above, what is effect of the call to `f1` at `Line A`?

## Answers:

1. Outputs some file information for all files found under `/home/user/work` directory,  
   with the exception of subdirectories that are called `.svn`
2. Outputs some file information for all files found under `/home/user/work` directory,  
   and each subdirectory named `CVS` is deleted from filesystem.
3. Outputs some file information for all files found under `/home/user/work` directory,  
   and each subdirectory named `.svn` is deleted from filesystem.
4. Outputs some file information for all files found under `/home/user/work` directory,  
   with the exception of subdirectories that are called `CVS`.
5. Outputs some file information for all files found under `/home/user/work` directory,  
   and file information from all subdirectories is omitted.    

## Correct:

> 1. Outputs some file information for all files found under `/home/user/work` directory,  
>    with the exception of subdirectories that are called `.svn` 
