## Sample code:
```python
x = 4
print(list(map(lambda y: y**2, range(1, x + 1))))
```

## Question:

Which represents the processing performed by the sample code above?

## Answers:

1. `[(2**1), (2**2), (2**3), (2**4)]`
2. `[(1**2), (2**2), (3**2), (4**2)]`
3. `[1, 4, 6, 8]`
4. `[x**2 for x in range(1, 4)]`
5. `[2*x for x in range(1, 4)]`

## Correct:

>@2. `[(1**2), (2**2), (3**2), (4**2)]`
