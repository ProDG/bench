## Sample code:

```python
tup = ('a', 'b', 'c', 'd', 'e')
```

## Question:

Based on the sample code above, which lines are legal operations?

## Answers:

1. `tup.delete('c')`  
   `print(tup[-4])`
2. `tup = [1, 2, 3]; tup[3] = 4`  
   `l = list(tup[2:4])`
3. `tup.remove('c')`  
   `tup.remove(2)`
4. `tup.remove('c')`  
   `l = list(tup[2:4])`
5. `print(tup[-4])`  
   `a = ['first', 'second', 'third']; [a1, a2, a3] = a`

## Correct:
>@5. `print(tup[-4])`  
>    `a = ['first', 'second', 'third']; [a1, a2, a3] = a`
