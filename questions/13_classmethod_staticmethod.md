```python
class A(object):
    def instance_monty(self, x):
        print("instance_monty: {}  arg: {}".format(self, x))
        
    @classmethod
    def class_brian(cls, x):
        print("class_brian: {}  arg: {}".format(cls, x))
        
    @staticmethod
    def static_wanda(x):
        print("static_wanda with arg {}".format(x))
```

## Question:
Based on the sample code above, which statement about the methods is correct?

## Answers:

1. An instance of class `A` must be created before `instance_monty` can be called, but `class_brian` ans `static_wanda` 
can be called without creating any instance of class `A`.
2. It is possible to call `instance_monty`, `static_wands` and `class_brian` without creating any instance of class `A`.
3. It is impossible to call any of these three methods without creating an instance of class `A`.
4. An instance of class `A` must be created before `instance_monty` and `static_wanda` can be called, but `class_brian`
can be called without creating any instance of class `A`.
5. An instance of class `A` must be created before `instance_monty` can be called, but `class_brian` and `static_wanda`
can be called only through the class.

## Correct:
> @1. An instance of class `A` must be created before `instance_monty` can be called, but `class_brian` ans `static_wanda` 
can be called without creating any instance of class `A`.

or (less likely)

> @2. It is possible to call `instance_monty`, `static_wands` and `class_brian` without creating any instance of class `A`.

(you can call `A.instance_monty(A, 'hey')` and it will work, despite it makes not much sense)
