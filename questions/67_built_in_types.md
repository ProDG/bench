## Question:

Which contains only valid built-in types?

## Answers:

1. `complex`  
   `tuple`  
   `char`
2. `float`  
   `frozenset`  
   `long`
3. `int`  
   `bytearray`  
   `frozenset`
4. `range`  
   `str`  
   `pointer`
5. `list`  
   `double`  
   `dict`
   
## Correct:

> @3. `int`  
>   `bytearray`  
>   `frozenset`
