```python
def foo(bool):
    if bool: yield 10
    else: return 
    yield 20

f = foo(False)
for i in f: print(i, end="")  # Line A: No output

f = foo(True)
for i in f: print (i, end=" ")  # Line B: Output is "10 20 "
```

#### Question:
Based on the sample code above, why is there no output from `Line A` while `Line B` gives `"10 20 "`?

#### Answers:
1. A generator defined with a `False` argument is always empty while a generator defined with `True` arguments is not.
2. `foo(False)` returns `None` and therefore no output at `Line A`.
3. The generator at `Line A` produces an empty list while generator at `Line B` produces `[10, 20]`.
4. The generator at `Line A` produces an empty tuple while generator at `Line B` produces `(10, 20)`.
5. The f generator at `Line A` is empty on the first try while the generator at `Line B` provides the first and second
yield values. 


#### Correct:
> 5. The f generator at `Line A` is empty on the first try while the generator at `Line B` provides the first and second
yield values. 

or 

> 1. A generator defined with a `False` argument is always empty while a generator defined with `True` arguments is not.

(I'm not sure)
