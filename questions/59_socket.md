## Sample code

```python
### server.py ###
import socket
sskt = socket.socket()
host = socket.gethostname()
port = 54321
sskt.bind((host, port))

sskt.listen(1)
c, addr = sskt.accept()
print('Connection from', addr)
# Line A
c.close()


### client.py ###
import socket

cskt = socket.socket()
host = socket.gethostname()
port = 54321

cskt.connect((host, port))
print(cskt.recv(1024))
cskt.close()
```

## Question:

Based on the sample code above, which code is needed at `Line A` to send a message to the client?

## Answers:

1. `c.send('Your connection succeeded.')`
2. `c.write('Your connection succeeded')`
3. `sskt.send(c, 'Your connection secceeded.')`
4. `c.send(b'Your connection secceeded.')`
5. `sskt.send(c, b'Your connection secceeded.')`

## Correct:

> @4. `c.send(b'Your connection secceeded.')`
