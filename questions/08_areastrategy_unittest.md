```python
from unittest import TestCase

class AreaStrategy(object):
    def calculate(self, x, y):
        if x > 0 and y > 0: return x * y
        if x < 0 or y < 0: raise ValueError('Cannot calculate the area of any negative numbers')
        else: return 0
        
class TestAreaStrategy(TestCase):
    def setUp(self):
        self._width = 30
        self._height = 144
        self._strategy = AreaStrategy()
        
    def testCalculate(self):
        strat = self._strategy
        self.assertEqual(self._width*self._height, strat.calculate(self._width, self._height))
        self.assertEqual(0, strat.calculate(self._width, 0))
        self.assertRaises(ValueError, strat.calculate, -self._width, self._height)
        self.assertRaises(ValueError, strat.calculate, -self._width, 0) 

if '__main__' == __name__:
    import unittest
    unittest.main()
```

#### Question:
When the sample code above is run, how many assertions and tests are executed, and how many test errors 
and failures occur?

#### Answers:

1. `Assestions: 4 / Tests: 1 / Errors: 1 / Failures: 0`
2. `Assestions: 4 / Tests: 1 / Errors: 1 / Failures: 1`
3. `Assestions: 2 / Tests: 1 / Errors: 0 / Failures: 1`
4. `Assestions: 4 / Tests: 1 / Errors: 0 / Failures: 0`
5. `Assestions: 2 / Tests: 1 / Errors: 1 / Failures: 1`

#### Correct:
> 4. `Assestions: 4 / Tests: 1 / Errors: 0 / Failures: 0`
