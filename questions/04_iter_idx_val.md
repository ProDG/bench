```python
def iter_idx_val(iterable):
    indices = range(len(iterable))
    for idx, val in zip(indices, iterable):
        yield idx, val
```

#### Question:
Which built-in function does the same as the `iter_idx_val` function in the sample code above?

#### Answers:

1. `__enumerate__`
2. `enumerate`
3. `enum`
4. `iteritems`
5. `iter`

#### Correct:

> 2. `enumerate`
