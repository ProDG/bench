## Sample code:

```python
def make_incrementer(n):        # Line 1
    def increment(x):           # Line 2
        return x + n            # Line 3
    return increment            # Line 4

add1 = make_incrementer(1)      # Line 5
print(add1(3))                  # Line 6
```

## Question:

Referring the sample code above, what feedback is given by the Python interpreter at `Line 3` after
being called from `Line 6`? 

## Answers:

1. A `NameError` is raised because `n` is not in the scope within increment function.
2. A `TypeError` is raised because non-numerics and numerics cannot be added together.
3. Printed to the standart output stream is '4'.
4. A `SyntaxWarning` was logged.
5. A `ValueError` is raised because `n` is not in scope within increment function.

## Correct:

> @3. Printed to the standart output stream is '4'.
