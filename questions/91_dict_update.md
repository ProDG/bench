## Sample code:
```python
dict1 = {'k1': 'v1', 'k2': 'v2', 'k3': 'v3'}
dict2 = {'k1': 'v1', 'k3': 'v3', 'k5': 'v5'}

dict3 = dict1.update(dict2)

print(dict1)  # {'k5': 'v5', 'k3': 'v3', 'k2': 'v2', 'k1': 'v1'}
print(dict2)  # {'k3': 'v3', 'k5': 'v5', 'k1': 'v1'} 
print(dict3)  # None
```

## Question:

Based on the sample code above, why is `dict3` `None`?

## Answers:

1. The `update` method of a dictionary object always returns `None`.
2. The `update` method of a dictionary object updates the dictionery with the key/value pairs from ohter, 
   overwriting, existing keys.
3. The `update` method of a dictionary object returns the updated dictionary.
4. There are no keys that are missing from both `dict1` and `dict2`.
5. `update()` accepts either another dictionary object or an iterable of key/value pairs, as tuples or other 
   iterables of length two.

## Correct:

>@1. The `update` method of a dictionary object always returns `None`.
