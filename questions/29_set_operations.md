## Sample code:

```python
set1 = set(['item_1', 'item_2', 'item_3', 'item_5', 'item_8'])
set2 = set(['item_2', 'item_3', 'item_5', 'item_7', 'item_11'])

print(sorted(set1.union(set2)))
print(sorted(set1.intersection(set2)))
print(sorted(set1.difference(set2)))
print(sorted(set1.symmetric_difference(set2)))
```

## Question:
What is the output of the sample code above?


## Answers:
1. `{'item_1', 'item_2', 'item_3', 'item_5', 'item_7', 'item_8', 'item_11'}`  
   `{'item_2', 'item_3', 'item_5'}`  
   `{'item_1', 'item_7', 'item_8', 'item_11'}`  
   `{'item_1', 'item_8'}`
2. `{'item_1', 'item_2', 'item_3', 'item_5', 'item_7', 'item_8', 'item_11'}`  
   `{'item_1', 'item_2', 'item_3', 'item_5'}`  
   `{'item_1', 'item_8'}`
   `{'item_1', 'item_7', 'item_8', 'item_11'}`  
3. `SyntaxError`
4. `{'item_2', 'item_3', 'item_5'}`  
   `{'item_1', 'item_2', 'item_3', 'item_5', 'item_7', 'item_8', 'item_11'}`  
   `{'item_1', 'item_8'}`
   `{'item_1', 'item_7', 'item_8', 'item_11'}`  
5. `{'item_1', 'item_11', 'item_2', 'item_3', 'item_5', 'item_7', 'item_8', }`  
   `{'item_2', 'item_3', 'item_5'}`  
   `{'item_1', 'item_8'}` 
   `{'item_1', 'item_11', 'item_7', 'item_8'}`  
     
## Correct:

> @5. `{'item_1', 'item_11', 'item_2', 'item_3', 'item_5', 'item_7', 'item_8', }`  
>   `{'item_2', 'item_3', 'item_5'}`  
>   `{'item_1', 'item_8'}`  
>   `{'item_1', 'item_11', 'item_7', 'item_8'}`

(Core difference: `item_11` goes right after `item_1`)
