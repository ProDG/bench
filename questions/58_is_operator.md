## Sample code:

```text
>>> [1, 2, 3] is [1, 2, 3]
False
>>> (1, 2, 3) is (1, 2, 3)
False
>>> "1, 2, 3" is "1, 2, 3"
True
```

## Question:

Base on the sample interactive session above, why are there different return values?

## Answers:

1. Lists are not evaluated as equal, because elements of lists may be changed in place.
2. Even though Strings are mutable object, `is` only checks the content.
3. Every sequence object is unique, even if two sequence objects contain the same elements.
4. Strings are immutable objects, and Python shares values for immutable values whenever possible.
5. Characters of both strings are the same and therefore both strings are the same.

## Correct:
> @5. Characters of both strings are the same and therefore both strings are the same.

## Hint
Another bullshit question. 

Actual proper answer is that `is` checks for objects identity (if both objects have the same object ID), while `==`
checks for object equality (values are the same).

Funny fact is, that `(1, 2, 3) is (1, 2, 3)` actually produces `True`, not `False` as stated in the 'sample session'.

Answer #5 looks as most correct. Because if two strings have equal content, Python uses for them the same ID (to
save memory and to make strings comparison faster). 

