## Question:

You use `import mystuff` instead of `from mystuff import xyz` to:

## Answers:

1. deny access to the constants defined in a module while allowing to its functions and classes.
2. avoid namespace clashes between imported modules.
3. allow you to redefine classes defined in an imported module.
4. import only symbols explicitly identified by the module author.
5. speed up attribute lookups by skipping the local scope.

## Correct:

> @2. avoid namespace clashes between imported modules.
