```python
import warnings

def deprecated(func):
    def newFunc(*args, **kwargs):
        warnings.warn('Call to deplecated function %s' % (func.__name__), category=DeprecationWarning)
        return func(*args, **kwargs)
        
    return newFunc    


@deprecated            # Line A
def prod(x, y):
    return x * y
    
# - - - - - - - - - -  # Line B    
```

## Question:

Which code inserted at `Line B` allows the code at `Line A` to be removed while keeping the functionality of the sample
code above unchanged?

## Answers:
1. `deprecated = prod`
2. `prod = deprecated()`
3. `prod(@deprecated)`
4. `prod = deprecated(prod)`
5. `@deprecated prod`

## Correct:
> @4. `prod = deprecated(prod)`
