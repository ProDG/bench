## Sample code:

```python
x = 7
dict1 = dict()
```

## Question:

Base on the sample code above, which is NOT a valid method of assigning a key/value pair to a dictionary?

## Answers:

1. `dict1 = {1, 4}`
2. `dict1 = {1: 4}`
3. `dict1[3] = 2+2`
4. `dict1[4+1] = 6`
5. `dict1[x] = 8`

## Correct:

> @1. `dict1 = {1, 4}`

## Hint:

`dict1 = {1, 4}` creates a set.
