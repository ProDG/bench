## Question:

Which methods must a custom class define to fully support the `+` operator?

## Answers:

1. `__add__()` and `__radd__()`
2. `__add__()` and `__sub__()`
3. `add()` and `radd()`
4. `add()` and `sub()`
5. `add()` and `__add__()`

## Correct:

> @1. `__add__()` and `__radd__()`

## Hint:
`__add__()` makes `+` working if custom class instance is the left (first) operand. To make it working
in case it's the right (last) operand, `__radd__()` should be defined.
