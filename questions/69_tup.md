## Question

Which does NOT create a tuple and assign it to `tup`?

## Answers:

1. `tup1 = (1, 2, 3, 4, 5)`  
   `tup2 = ('a', 'b', 'c', 'd', 'e')`  
   `tup = (tup1, tup2)`
2. `tup = tuple(1, 2, 3, 4, 5)`
3. `tup = ('a', 'b', 'c', 'd', 'e')`
4. `tup = (1, 2, 3, 4, 5)`
5. `tup = "1a", "2b", "3c", "4d", "5e"`

## Correct:
> @2. `tup = tuple(1, 2, 3, 4, 5)`
