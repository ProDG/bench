## Question:

Which statement describes a result of defining a `__slots__` class variable in a class?

## Answers:

1. Classes defining `__slots__` automatically support weak references to its instances.
2. By default, classes have a dictionary for attribute storage. This wastes space for objects having
   very few instance variables, specifically when creating large numbers of instances. This is avoided
   through the use of `__slots__`.
3. If defined in a class, `__slots__` reserves space for the declared variables and automatically creates
   a `__dict__` for each instance.
4. Instances cannot be assigned new variables listed in the `__slots__` definition. Attempts to assign
   to a listed variable name raises an `AttributeError`.
5. `__slots__` are implemented at the instance level by creating descriptors for each variable name and automatically
   provide the benefits of using descriptors for instance attributes.
   
## Correct:
>@2. By default, classes have a dictionary for attribute storage. This wastes space for objects having
>    very few instance variables, specifically when creating large numbers of instances. This is avoided
>    through the use of `__slots__`.


