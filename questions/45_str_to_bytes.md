## Question:

Which encodes the str `"mystr"` to bytes?

## Answers:

1. `mystr.encode('bytes')`
2. `bytes.encode(mystr, 'bytes')`
3. `import string`  
   `string.encode(mystr, 'bytes')`
4. `str.encode(mystr)`
5. `import string`  
   `string.encode(mystr)`

## Correct:
> @4. `str.encode(mystr)`

## Hint:

The same result can be achieved with `mystr.encode()`. `string` module contains some string constants and is not
related to encoding/decoding.
