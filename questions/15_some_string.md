```python
def g(): g.s = "some string"

print(g.__dict__)
print(g.s)
g()
print(g.s)
print(g.__dict__)
```

## Question:
Why does the sample code above raise an error?

## Answers:

1. The `s` attribute of `g` is not defined until `g` is executed.
2. Function `g` creates a new local variable also names `g` and then creates the `s` attribute for it.
3. The `g` function does not have a return statement.
4. The function body of `g` if not indented below the function definition line.
5. The `__dict__` of `g` is not defined until `g` is executed.

## Correct:
> @1. The `s` attribute of `g` is not defined until `g` is executed.
