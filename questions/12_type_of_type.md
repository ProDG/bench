```python
class A: pass
x = 'wow'
def f(): pass

# t = type(A)  # Line A
# t = type(x)  # Line B
# t = type(f)  # Line C

print(type(t))
```

#### Question:
Based on the sample code above, whi this the outpue `<class 'type'>` the same when either `Line A`, `Line B` or `Line C`
is uncommented?

#### Answers:

1. Python is untyped language.
2. The Python interpreter chacks types at runtime.
3. All Python classes inherit from `object`.
4. `type(A)`, `type(x)` and `type(f)` are identical.
5. All Python types are `type` objects.

#### Correct:
>@5. All Python types are `type` objects.
