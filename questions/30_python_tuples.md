## Sample code:

```python
tot1 = ((1, 'a'), (2, 'b'), (3, 'c'), (4, 'd'), (5, 'e'))
tot2 = ((6, 'f'), (7, 'g'), (8, 'h'), (9, 'i'), (10, 'j'))
tot3 = ((11, 'k'), (12, 'l'), (13, 'm'), (14, 'n'), (15, 'o'))
tot4 = ((16, 'p'), (17, 'q'), (18, 'r'), (19, 's'), (20, 't'))
tot5 = ((21, 'u'), (22, 'v'), (23, 'w'), (24, 'x'), (25, 'y'))
tot6 = ((26, 'z')) 

tot = (tot1, tot2, tot3, tot4, tot5, tot6)
```

## Question:

Based o the sample code above, which displays `'p y t h o n'`?

## Answers:

1. `print(tot[15], tot[24], tot[20], tot[7], tot[14], tot[13])`
2. `print(tot[3][0], tot[4][4], tot[3][4], tot[1][2], tot[2][4], tot[2][3])`
3. `print(tot[1][0][3], tot[1][4][4], tot[1][4][3], tot[1][2][1], tot[1][4][2], tot[1][3][2])`
4. `print(tot[16], tot[25], tot[21], tot[8], tot[15], tot[14])`
5. `print(tot[3][0][1], tot[4][4][1], tot[3][4][1], tot[1][2][1], tot[2][4][1], tot[2][3][1])`

## Correct:

> @5 `print(tot[3][0][1], tot[4][4][1], tot[3][4][1], tot[1][2][1], tot[2][4][1], tot[2][3][1])`

## Hint:

Only correct answer has `[1]` as the latest element selector, which matched data structure.
