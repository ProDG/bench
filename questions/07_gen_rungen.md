## Sample code:
```python
def gen():
    a, b = 0, 1
    print("START", end=" ")
    for i in range(4):
        yield b
        a, b = b, a+b
        
    print("END", end=" ")
    
def rungen():
    G = gen()
    
    while True:
        try:
            v = next(G)
            print(v, end=" ")
        except StopIteration:
            break

rungen()    # Line A output START 1 1 2 3 END
print()
for v in gen(): print(v, end=" ")   # Line B output START 1 1 2 3 END  
```

## Question:

Based on the sample code above, what is the difference in execution between `Line A` and `Line B`?

## Answers:

1. `Line A` calls a function which runs code that is equivalent to `Line B`.
2. `Line A` relies on the generator nature of `gen()` while `Line B` does not.
3. `Line A` invokes `gen()` one too many times so it must handle the `StopIteration` error, while `Line B` stops
    before `StopIteration` is encountered.
4. `Line B` calls a function which runs code that is equivalent to `Line A`.
5. `Line B` invokes `gen()` one too mane times so it must handle the `StopIteration` error, while `Line A` stops
before `StopIteration` is encountered.

#### Correct:
> @4. `Line B` calls a function which runs code that is equivalent to `Line A`.

## Hint:
I'm not sure! Maybe:
> @1. `Line A` calls a function which runs code that is equivalent to `Line B`.
?
