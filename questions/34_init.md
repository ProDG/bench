## Sample code:
```python
class A:
    def __init__(self, n):
        self.n = n
        
a = A(1)
print(a.n)  # Output: 1
```

## Question:

What is the purpose of the `__init__` method in the sample code above?

## Answers:

1. When a class defines an `__init__()` method, class instantiation automatically invokes `__init__()` for the 
newly-created class instance.
2. When a class includes an `__init__()` method, class creation automatically invokes `__init__()` for the 
newly-created class.
3. `self` is a reference to the instance of the class.
4. The `__init__` method is required for all class definitions and is used to define attributes.
5. The `__init__` magic method is required to satisfy the iterator protocol.

## Correct:

> @1. When a class defines an `__init__()` method, class instantiation automatically invokes `__init__()` for the 
> newly-created class instance.
