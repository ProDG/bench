## Sample code:

```python
str = "The quick brown fox jumps over the {0} lazy dogs"

print(str.format(1+2))  # Line A
```

## Question:

Based on the sample code above, which replacement for `Line A` produces the same results as the original `Line A`?

## Answers:

1. `print("The quick brown fox jumps over the %d lazy dogs" % (1+2))`
2. `print("The quick brown fox jumps over ths %s lazy dogs", "3")`
3. `print(str.format("1+2")`
4. `print(str.format("%f" % (1+2)))`
5. `print("The quick brown fox jumps over the %s lazy dogs", 1+2)`

## Correct:
> @1. `print("The quick brown fox jumps over the %d lazy dogs" % (1+2))`

## Hint:
The only proper use of `%`.

