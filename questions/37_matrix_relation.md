## Sample code:

```python
a = [
#    C  C  C
#    1  2  3
    [1, 2, 3],  # R 1
    [4, 5, 6],  # R 2
    [7, 8, 9]   # R 3
]

b = [[row[i] for row in a] for i in range(len(a))]
```

## Question:

Which describes the relationship of `b` to `a` in the sample code above?

## Answers:

1. The rows of `b` are in the reverse order of the rows of `a`.
2. The columns of `a` are used as the rows of `b`.
3. The columns of `b` are used as the rows of `a`.
4. Both `a` and `b` have non-zero determinants.
5. The columns of `b` are in the reverse order or the columns of `a`.

## Correct:

> @2. The columns of `a` are used as the rows of `b`.

