## Question:

Which methods are defined for an iterator class?

## Answers:

1. `__iter__`, `__gen__`, `next`
2. `__iter__`, `next`
3. `__iter__`, `__next__`
4. `iter`, `has_next`, `next`
5. `iter`, `next`

## Correct:

> @3. `__iter__`, `__next__`
